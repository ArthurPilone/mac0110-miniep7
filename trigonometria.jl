#____________________________________________________________________
#MAC0110 - MiniEP7
#Arthur Pilone M. da Silva - NUSP 11795450
#--------------------------------------------------------------------

#--------------------------------{OBS}-------------------------------
#       Para que não haja confusão entre as funções trigonométricas
#   originais do Julia, com as feitas neste minieEP, as funções
#   do miniEP terão seus nomes em português (sin(x) = seno(x),
#   cos(x) = cosseno(x) e tan(x) = tangente(x)).

#   P.S. Mensagem acima escrita antes da parte final da troca de nomes :/
#----------------------------------^---------------------------------

#--------------------------------------------------------------------
#                               Parte 1
#____________________________________________________________________

#   função auxiliar, que calcula (x^n)/n! de maneira a evitar valores
#   muito grandes; (Com n maior ou igual a 2)

function powFact(x, n)
    resultado = BigFloat(x, RoundDown, precision = 64)
    i = 2
    while (i <= n)
        resultado = resultado * (x / i)
        i += 1
    end
    return resultado
end

function seno(x)
    soma = BigFloat(x, RoundDown, precision = 64)
    i = 3
    pos = false
    while (i <= 19)
        if (pos)
            soma += powFact(x, i)
        else
            soma -= powFact(x, i)
        end
        i += 2
        pos = !pos
    end
    return BigFloat(soma, RoundUp, precision = 32)
end

function cosseno(x)
    soma = BigFloat(1, RoundDown, precision = 64)
    i = 2
    pos = false
    while (i <= 18)
        if (pos)
            soma += powFact(x, i)
        else
            soma -= powFact(x, i)
        end
        i += 2
        pos = !pos
    end
    return BigFloat(soma, RoundDown, precision = 32)
end

#Função dada no enunciado do miniEP

function bernoulli(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0:n
        A[m+1] = 1 // (m + 1)
        for j = m:-1:1
            A[j] = j * (A[j] - A[j+1])
        end
    end
    return abs(A[1])
end

function tangente(x)
    soma = BigFloat(x, RoundDown, precision = 64)
    i = 2
    while i <= 15
        soma +=
            (powFact(2, 2 * i) * ((2^(2i)) - 1) * bernoulli(i) * (x^(2i - 1)))
        i += 1
    end
    return BigFloat(soma, RoundUp, precision = 32)
end

#--------------------------------------------------------------------
#                               Parte 2
#____________________________________________________________________

function quaseigual(v1,v2)
    if(abs(v1-v2) < 0.0001)
        return true
    else
        return false
    end
end

function check_sin(value,angle)
    if(quaseigual(value,sin(angle)))
        return true
    else
        return false
    end
end

function check_cos(value,angle)
    if(quaseigual(value,cos(angle)))
        return true
    else
        return false
    end
end

function check_tan(value,angle)
    if(quaseigual(value,tan(angle)))
        return true
    else
        return false
    end
end

function taylor_sin(x)
    return seno(x)
end
function taylor_cos(x)
    return cosseno(x)
end
function taylor_tan(x)
    return tangente(x)
end

#--------------------------------------------------------------------
#                          Parte 3 - Testes
#____________________________________________________________________

using Test
function test()
    @test check_sin(0.5,π/6)
    @test check_sin(0.7071,π/4)
    @test check_sin(0.866,π/3)
    @test check_sin(0,π)
    @test check_sin(1,π/2)
    @test check_cos(0.5,π/3)
    @test check_cos(0.866,π/6)
    @test check_cos(-1,π)
    @test check_cos(0,π/2)
    @test check_cos(1,0)
    @test check_cos(-0.5,2π/3)
    @test check_cos(-0.7071,3π/4)
    @test check_cos(-0.866,5π/6)
    @test check_tan(0.57735,π/6)
    @test check_tan(-0.57735,5π/6)
    @test check_tan(1,π/4)
    @test check_tan(-1.732,2π/3)

    @test check_sin(taylor_sin(π/3),π/3)
    @test check_sin(taylor_sin(π/4),π/4)
    @test check_sin(taylor_sin(π/6),π/6)
    @test check_sin(taylor_sin(π),π)
    @test check_cos(taylor_cos(0),0)
    @test check_cos(taylor_cos(π/4),π/4)
    @test check_cos(taylor_cos(5π/6),5π/6)
    @test check_cos(taylor_cos(π/3),π/3)
    @test check_tan(taylor_tan(π/3),π/3)
    @test check_tan(taylor_tan(π/4),π/4)
    @test check_tan(taylor_tan(π/6),π/6)
end
test()
